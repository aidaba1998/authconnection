import firebase from 'firebase';


const firebaseConfig = {
    apiKey: "AIzaSyBONrznPwshp6Zdf1TEIPT3ivGIkV0dqgw",
    authDomain: "connexion-2da17.firebaseapp.com",
    projectId: "connexion-2da17",
    storageBucket: "connexion-2da17.appspot.com",
    messagingSenderId: "248402879154",
    appId: "1:248402879154:web:d4d26a12dd2ef506ce1fab"
  };
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
const auth = firebase.auth();
export { auth };
export default db;